import React from 'react';

const Pagination = () => {
  return (
    <div className="row listingPagination headerPagination">
      <div className="column medium-4 pagingSide paging-col-1">
        <div className="paging-position">
          Found 47 Results <br />
          Page 1 of 3
        </div>
      </div>
      <div className="column medium-4 paging-col-2">
        <div className="inline-paging">
          <span className="currentPage active">
            <strong>1</strong>
          </span>
          <span className="paginationOn">
            <a href="/gb/cambridgeenglish/catalog/english-academic-purposes/product-catalogue?layout=&amp;page=2"
          className="ajaxEnabled">2</a>
          </span>
          <span className="paginationOn">
            <a href="/gb/cambridgeenglish/catalog/english-academic-purposes/product-catalogue?layout=&amp;page=3"
          className="ajaxEnabled">3</a>
          </span>
        </div>
      </div>
      <div className="column medium-4 dropdownSide paging-col-3">
        <ul className="buttons">
          <li className="previous not-active">
            <a className="ajaxEnabled">Previous</a>
          </li>
          <li className="next ">
            <span>
              <a className="paginationOn ajaxEnabled" href="/gb/cambridgeenglish/catalog/english-academic-purposes/product-catalogue?layout=&amp;page=2">Next</a>
            </span>
          </li>
        </ul>
      </div>
    </div>
  )
};

export default Pagination;
