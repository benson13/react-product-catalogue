import React from 'react';

const FilterItem = props => {

  return (
    <div className="fieldItem">
      <input id="checkbox1" type="checkbox" />
      <label for="checkbox1" >{props.facetItem}</label>
    </div>
  )
};

export default FilterItem;