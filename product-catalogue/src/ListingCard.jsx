import React from 'react';
import faker from 'faker';
import ContentDetails from './ContentDetails';
import Pagination from './Pagination';
import ListingItem from './ListingItem'
import ActionAddCart from './ActionAddCart';
import ActionViewProduct from './ActionViewProduct';

const ListingCard = () => {
  return (
    <div className="people-you-might-know">
      <div className="add-people-header">
        <h6 className="header-title">
          <b>All titles</b>
        </h6>
      </div>
      <Pagination />
      <ListingItem>
        <ContentDetails
          jacket = {faker.internet.avatar()}
          title = {faker.commerce.productName()}
        />
        <ActionAddCart />
      </ListingItem>
      <ListingItem>
        <ContentDetails
          jacket={faker.internet.avatar()}
          title={faker.commerce.productName()}
          author = {faker.name.firstName() + ' ' + faker.name.lastName()}
        />
        <ActionAddCart />
      </ListingItem>
      <ListingItem>
        <ContentDetails
          jacket={faker.image.animals()}
          title={faker.commerce.productName()}
          publicationDate = {faker.date.month() + ' ' + '2000s'}
          author = {faker.name.firstName() + ' ' + faker.name.lastName()}
        />
        <ActionViewProduct />
      </ListingItem>
      <ListingItem>
        <ContentDetails
          jacket={faker.image.cats()}
          title={faker.commerce.productName()}
          publicationDate = {faker.date.month() + ' ' + '2000s'}
        />
        <ActionViewProduct />
      </ListingItem>
      <ListingItem>
        <ContentDetails
          jacket={faker.image.sports()}
          title={faker.commerce.productName()}
          publicationDate = {faker.date.month() + ' ' + '2000s'}
          author = {faker.name.firstName() + ' ' + faker.name.lastName()}
        />
        <ActionViewProduct />
      </ListingItem>
    </div>
  );
};

export default ListingCard;