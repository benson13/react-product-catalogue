import React from 'react';

const FilterResults = props => {
  console.log(props);
  return (
    <div className="filter">
      <h4>{props.subTitle}</h4>
      <div className="filterItems">
        <fieldset className="facet-items">
          {props.children}
          <a href="/">Show more</a>
        </fieldset>
      </div>

      
    </div>

    
  );

}


export default FilterResults;