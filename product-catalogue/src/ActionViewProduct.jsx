import React from 'react';

const ActionViewProduct = () => {
  return(
    <div className="small-12 medium-6 columns add-friend">
      <div className="add-friend-action">
        <button className="button secondary small">
          <i className="fa fa-user-plus" aria-hidden="true"></i>
          View Product
        </button>
      </div>
    </div>
  );
};

export default ActionViewProduct;