import React from 'react';

const ActionAddCart = () => {
  return(
    <div className="small-12 medium-6 columns add-friend">
      <div className="add-friend-action">
        <button className="button primary small">
          <i className="fa fa-user-plus" aria-hidden="true"></i>
          Add to Cart
        </button>
      </div>
    </div>
  );
};

export default ActionAddCart;