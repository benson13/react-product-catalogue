import React from 'react';
import ReactDOM from 'react-dom';
import FilterResults from './FilterResults';
import FilterItem from './FilterItem'; 
import ListingCard from './ListingCard';

const ListingCardLayout = () => {
  return (
    <ListingCard />
  );
};

const a = ["American English", "British English", "International English"];
const b = [" (14)", " (27)", " (2)"];

var subjects = [{
  subject: 'American English',
  count: 27
}, {
    subject: 'Bristish English',
    count: 14
  }, {
    subject: 'International English',
    count: 5
  } 
]

var bookFormat = [{
  format: 'Book',
  count: 18
}, {
  format: 'Audio',
  count: 16
}, {
  format: 'Online & Online Blended',
  count: 37
  }, {
    format: 'Mixed Media',
    count: 11
  }
]

const Filter = () => {
  return (
    <div>
      <h3>Refine results</h3>
      <FilterResults
        mainTitle="Refine results"
        subTitle="English type"
      >

        {subjects.map(function(i, index) {
          return <FilterItem key={i} facetItem={i.subject + " (" + i.count + ")"} />
        })}
        
      </FilterResults>

      <FilterResults 
        subTitle="Format"
      >
        {bookFormat.map(function (i, index) {
          return <FilterItem key={i} facetItem={i.format + " (" + i.count + ")"} />
        })}
      </FilterResults>
    </div>
    
  );
};
ReactDOM.render(<ListingCardLayout />, document.getElementById('main-area'));
ReactDOM.render(<Filter />, document.getElementsByClassName('filter-wrapper')[0]);
