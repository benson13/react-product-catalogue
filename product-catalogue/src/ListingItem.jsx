import faker from "faker";
import React from 'react';
import ActionAddCart from './ActionAddCart';
import ActionViewProduct from './ActionViewProduct';

const ListingItem = props => {
  return(
    <div className="row add-people-section">
      {props.children}
    </div>
  );
};

export default ListingItem;