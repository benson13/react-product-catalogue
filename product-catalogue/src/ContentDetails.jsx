import React from 'react';

const ContentDetails = props => {
  return (
    <div className="small-12 medium-6 columns about-people">
      <div className="about-people-avatar">
        <img className="avatar-image" src={props.jacket} alt="Sample Jacket" />
      </div>
      <div className="about-people-author">
        <p className="author-name">
          <a href="#">{props.title}</a>
        </p>
        <p className="author-location">
          <i className="fa fa-map-marker" aria-hidden="true"></i>
          {props.publicationDate && <strong>Publication Date: </strong>}{props.publicationDate}
        </p>
        <p className="author-mutual">
          {props.author && <strong>Author: </strong>}{props.author}
        </p>
      </div>
    </div>
  );
};

export default ContentDetails;